<?php

$errors_arr = array(
	1 => 'Error en Peticion',
	2 => 'Valores no definidos',
	3 => 'Error al subir archivo',
	4 => 'Extension no valida',
	5 => 'Tamaño no valido',
	6 => 'No existe el folder'
);

$extensions_arr = array('.jpg', '.png');

function db_connect($dbhost=null, $dbuser=null, $dbpass=null, $dbname=null) 
{
	try
	{
		$dbh= new PDO("mysql:host=".$dbhost.";dbname=".$dbname."", $dbuser, $dbpass);
		$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		return $dbh;
	}catch (PDOException $e) {
		return $e->getMessage();
	}
}

function __fetchAll($sql='') 
{
	try 
	{
		$dbh= db_connect(BD_HOST,BD_USER,BD_PASSWORD,BD_DATABASE);
		$q= $dbh->prepare($sql);
	  $q->execute();
	  $result= $q->fetchAll(PDO::FETCH_ASSOC);
	  return $result;
	} catch (PDOException $e) {
		return $e->getMessage();
	}
}

function __fetch($sql='') 
{
	try
	{
		$dbh= db_connect(BD_HOST,BD_USER,BD_PASSWORD,BD_DATABASE);
		$q= $dbh->prepare($sql);
	  $q->execute();
	  $result= $q->fetch(PDO::FETCH_ASSOC);
	  return $result;
	} catch (PDOException $e) {
		return $e->getMessage();
	}
}

function __insert($sql='') 
{
	try 
	{
		$dbh= db_connect(BD_HOST,BD_USER,BD_PASSWORD,BD_DATABASE);
		$q= $dbh->prepare($sql);
	  $q->execute();
	  return $q;
	} catch (PDOException $e) {
		return $e->getMessage();
	}
}

function clean($str=null)
{
	$str= trim($str);
	if(get_magic_quotes_gpc()) {
		$str= (string) addslashes($str);
	}
	return (string) stripslashes($str);	
}

function generateKey()
{
	$a= array(2,4,6,8,10,12,14,16,18,20);
	$b= array(1,3,5,7,9,13,15,17,19,21);
	$c= array(0,32,54,76,98,67,54,31,02);
	return $key;
}