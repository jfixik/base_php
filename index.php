<?php

require 'Slim/Slim.php';

require 'config/helpers.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

require 'config.inc.php';

require 'includes.php';

require 'routes.php';

$app->run();